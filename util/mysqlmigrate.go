package util

import (
	"log"

	"github.com/jmoiron/sqlx"
	"github.com/pressly/goose"
)

const (
	DBParams       = `interpolateParams=true&parseTime=true&sql_mode="ANSI,NO_AUTO_VALUE_ON_ZERO,ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION"&sql_notes=false&clientFoundRows=true`
	DBParamsBackup = `interpolateParams=true&parseTime=true&sql_mode="ANSI,NO_AUTO_VALUE_ON_ZERO,ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION,NO_ZERO_DATE,NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO"&sql_notes=false&clientFoundRows=true`
)

func MysqlConnect(connectionString string) (*sqlx.DB, error) {
	var db *sqlx.DB
	var err error

	db, err = sqlx.Connect("mysql", connectionString+"?"+DBParams)

	if err != nil {
		if db != nil {
			db.Close()
		}
		log.Println("Warning in mysql: ", err)

		return sqlx.Connect("mysql", connectionString+"?"+DBParamsBackup)
	}

	return db, err
}

func MysqlMigrateUp(connectionString string, migrateDir string) error {
	goose.SetDialect("mysql")
	db, err := MysqlConnect(connectionString)
	if err != nil {
		log.Printf("Failed to connect to mysql with connectionString: %s \n %v", connectionString, err)
		return err
	}

	return goose.Up(db.DB, migrateDir)
}
func MysqlMigrateDown(connectionString string, migrateDir string) error {
	goose.SetDialect("mysql")
	db, err := MysqlConnect(connectionString)
	if err != nil {
		return err
	}

	return goose.Down(db.DB, migrateDir)
}

func MysqlMigrateUpTo(connectionString string, migrateDir string, toVersion int64) error {
	goose.SetDialect("mysql")
	db, err := MysqlConnect(connectionString)
	if err != nil {
		return err
	}

	return goose.UpTo(db.DB, migrateDir, toVersion)
}

func MysqlMigrateDownTo(connectionString string, migrateDir string, toVersion int64) error {
	goose.SetDialect("mysql")
	db, err := MysqlConnect(connectionString)
	if err != nil {
		return err
	}

	return goose.DownTo(db.DB, migrateDir, toVersion)
}
