package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/widlundtobias/dog-server/dogapi"
	"gitlab.com/widlundtobias/dog-server/dogdata"
	"gitlab.com/widlundtobias/dog-server/util"

	"github.com/husobee/vestigo"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	_ "github.com/go-sql-driver/mysql"
)

type Configuration struct {
	Port               string
	DatabaseConnection string
	MigrateDir         string
	StorageMode        string
}

var rootCommand = &cobra.Command{
	Use:   "server",
	Short: "Start dog server",
	RunE:  rootCmdFunc,
}

func rootCmdFunc(cmd *cobra.Command, args []string) error {
	router := vestigo.NewRouter()
	// you can enable trace by setting this to true
	vestigo.AllowTrace = true

	// Setting up router global  CORS policy
	// These policy guidelines are overriddable at a per resource level shown below
	router.SetGlobalCors(&vestigo.CorsAccessControl{
		AllowOrigin:      []string{"*", "test.com"},
		AllowCredentials: true,
		ExposeHeaders:    []string{"X-Header", "X-Y-Header"},
		MaxAge:           3600 * time.Second,
		AllowHeaders:     []string{"X-Header", "X-Y-Header"},
	})

	cfg := &Configuration{}

	if err := viper.Unmarshal(cfg); err != nil {
		return err
	}

	//services
	var dogService dog.Service

	//initialise services
	switch cfg.StorageMode {
	case "mysql":
		//database migration
		if err := util.MysqlMigrateUp(cfg.DatabaseConnection, cfg.MigrateDir); err != nil {
			return err
		}

		//create database instance that services will use
		db, err := util.MysqlConnect(cfg.DatabaseConnection)
		if err != nil {
			return err
		}

		dogService = dog.NewSQLService(db)
	case "dummy":
		//dogService = dog.NewDummySerbice(db)
	default:
		return fmt.Errorf("Invalid storageMode: %s", cfg.StorageMode)
	}
	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		dogService.Close(ctx)
		cancel()
	}()

	//managers
	dogManager := dog.NewManager(dogService)

	//api
	dogAPI := dogapi.New(dogManager)

	//routes
	dogAPI.RegisterRoutes(router)
	//if you also serve static files from this application:
	//router.Handle("/**", staticFs)

	// Below Applies Local CORS capabilities per Resource (both methods covered)
	// by default this will merge the "GlobalCors" settings with the resource
	// cors settings.  Without specifying the AllowMethods, the router will
	// accept any Request-Methods that have valid handlers associated
	router.SetCors("/welcome", &vestigo.CorsAccessControl{
		AllowMethods: []string{"GET"},                    // only allow cors for this resource on GET calls
		AllowHeaders: []string{"X-Header", "X-Z-Header"}, // Allow this one header for this resource
	})

	//log.Fatal(http.ListenAndServeTLS(":" + cfg.Port, "MyCertificate.crt", "MyKey.key", router))
	log.Fatal(http.ListenAndServe(":"+cfg.Port, router))

	return nil
}

func initConfig() {
	viper.SetDefault("Port", "8080")
	viper.SetDefault("DatabaseConnection", "root:secret@tcp(localhost:3306)/dog")
	viper.SetDefault("MigrateDir", "./migrations")
	viper.SetDefault("StorageMode", "mysql")

	var cfgFile string

	rootCommand.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is config.toml)")

	failOnMissingConfig := false
	if cfgFile != "" {
		failOnMissingConfig = true
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath(".")
		viper.SetConfigName("config")
	}

	err := viper.ReadInConfig()            //find and read the config file
	if failOnMissingConfig && err != nil { // Handle errors reading the config file
		log.Fatal("Failed to read config", err)
	}
}

func main() {
	cobra.OnInitialize(initConfig)

	if err := rootCommand.Execute(); err != nil {
		os.Exit(1)
	}
}
