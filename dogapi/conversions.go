package dogapi

import dog "gitlab.com/widlundtobias/dog-server/dogdata"

func toDog(d *dog.Dog) (result Dog) {
	result.Id = int64(d.ID)
	result.Name = d.Name
	result.Age = d.Age
	result.Breed = d.Breed
	result.PettedCount = d.PettedCount
	return
}

func toDogs(dogs dog.Dogs) (result []Dog) {
	result = make([]Dog, len(dogs), len(dogs))

	for index, _ := range dogs {
		result[index] = toDog(dogs[index])
	}

	return
}
