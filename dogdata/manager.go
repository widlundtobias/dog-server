package dog

import (
	"context"

	"github.com/pkg/errors"
)

type Manager struct {
	service Service
}

func NewManager(service Service) *Manager {
	manager := &Manager{}
	manager.service = service
	return manager
}

func (m *Manager) Dogs() (Dogs, error) {
	return m.service.List(context.Background())
}

func (m *Manager) Get(id uint64) (*Dog, error) {
	return m.service.Get(context.Background(), id)
}

func (m *Manager) Create(dog *Dog) error {
	return m.service.Create(context.Background(), dog)
}

func (m *Manager) Pet(id uint64) (int, error) {
	dog, err := m.service.Get(context.Background(), id)

	if err != nil {
		return -1, errors.Wrapf(err, "Cannot pet dog %d", id)
	}

	dog.PettedCount += 1
	err = m.service.Update(context.Background(), dog)

	if err != nil {
		return -1, errors.Wrapf(err, "Cannot pet dog %d", id)
	}

	return dog.PettedCount, nil
}
