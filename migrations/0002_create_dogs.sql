-- +goose Up
CREATE TABLE `dogs` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` MEDIUMTEXT NOT NULL,
  `age` INT NOT NULL,
  `breed` MEDIUMTEXT NOT NULL,
  `petted_count` INT NOT NULL,
  PRIMARY KEY (`id`))
 ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- +goose Down
DROP TABLE `dogs`;
