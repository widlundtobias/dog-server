package dogapi

import (
	"net/http"

	"github.com/husobee/vestigo"
	dog "gitlab.com/widlundtobias/dog-server/dogdata"
)

type DogAPI struct {
	dogManager *dog.Manager
}

func New(dogManager *dog.Manager) (d *DogAPI) {
	d = new(DogAPI)
	d.dogManager = dogManager
	return
}

func (d *DogAPI) RegisterRoutes(router *vestigo.Router) {
	router.Get("/api/dogs", d.GetDogsHandler)
	router.Get("/api/dogs/breed/:id1/:id2/:name", d.BreedDogsHandler)
	router.Get("/api/dogs/pet/:id", d.PetDogHandler)
}

func (d *DogAPI) GetDogsHandler(w http.ResponseWriter, r *http.Request) {

	dogs, err := d.dogManager.Dogs()

	if err != nil {
		writeErrorResponse(w, newErrorResponse(500, "Error listing dogs: "+err.Error()))
		return
	}

	response := DogsResponse{toDogs(dogs)}

	writeOKResponse(w, response)
}

func (d *DogAPI) BreedDogsHandler(w http.ResponseWriter, r *http.Request) {

	id1Str := vestigo.Param(r, "id1")
	id2Str := vestigo.Param(r, "id2")
	name := vestigo.Param(r, "name")

	id1, errResp := argToUInt64(id1Str)
	if errResp != nil {
		writeErrorResponse(w, errResp)
		return
	}

	id2, errResp := argToUInt64(id2Str)
	if errResp != nil {
		writeErrorResponse(w, errResp)
		return
	}

	if id1 == id2 {
		writeErrorResponse(w, newErrorResponse(400, "cannot breed with itself"))
		return
	}

	dad, err := d.dogManager.Get(id1)
	if err != nil {
		writeErrorResponse(w, newErrorResponse(400, err.Error()))
		return
	}

	mom, err := d.dogManager.Get(id2)
	if err != nil {
		writeErrorResponse(w, newErrorResponse(400, err.Error()))
		return
	}

	bred := dog.Breed(dad, mom, name)
	err = d.dogManager.Create(bred)

	if err != nil {
		writeErrorResponse(w, newErrorResponse(500, "Error creating new bred dog: "+err.Error()))
		return
	}

	writeOKResponse(w, bred)
}

func (d *DogAPI) PetDogHandler(w http.ResponseWriter, r *http.Request) {

	idStr := vestigo.Param(r, "id")

	id, errResp := argToUInt64(idStr)
	if errResp != nil {
		writeErrorResponse(w, errResp)
		return
	}

	newPetCount, err := d.dogManager.Pet(id)

	if err != nil {
		writeErrorResponse(w, newErrorResponse(400, err.Error()))
		return
	}

	resp := PetDogResponse{
		Id: int64(id), NewPetCount: newPetCount,
	}
	writeOKResponse(w, resp)
}

type Dog struct {
	Id          int64  `json:"id"`
	Name        string `json:"name"`
	Age         int    `json:"age"`
	Breed       string `json:"breed"`
	PettedCount int    `json:"petted_count"`
}

type DogsResponse struct {
	Dogs []Dog `json:"dogs"`
}

type PetDogResponse struct {
	Id          int64 `json:"id"`
	NewPetCount int   `json:"new_pet_count"`
}
