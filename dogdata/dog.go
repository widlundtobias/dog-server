package dog

type Dogs []*Dog

type Dog struct {
	ID          uint64
	Name        string
	Age         int
	Breed       string
	PettedCount int `db:"petted_count"`
}

func Breed(dad *Dog, mom *Dog, name string) (child *Dog) {
	child = &Dog{
		Name:        name,
		Age:         0,
		Breed:       dad.Breed[:len(dad.Breed)/2] + mom.Breed[len(mom.Breed)/2:],
		PettedCount: 0,
	}
	return
}
